'use strict';

var React = require('react');

var TableRow = React.createClass({

  componentDidMount: function(){
  },

  render: function(){
    return (
      <tr>
       <td>{this.props.content}</td>
      </tr>
    );
  }
});

module.exports = TableRow;