'use strict';

var React = require('react');
var TableRow = require('./table_row.jsx');

var MyTable = React.createClass({

  componentDidMount: function(){
  },

  getDefaultProps: function(){
    return {posts: []}
  },

  render: function(){

    this.rows = [];

    var i = 1;
    Object.keys(this.props.posts).forEach( (key) => {
      this.rows.push(
        <TableRow key={i} content={this.props.posts[key].body} />
      );
      i++;
    }, this);

    return (
      <table>
        <tbody>
        {this.rows}
        </tbody>
      </table>
    );
  }
});

module.exports = MyTable;