'use strict';

var React = require('react');
var RouterView = require('./router.jsx');

var AppView = React.createClass({

  componentDidMount: function(){
  },

  render: function(){
    return (
      <div className="app-view">
        <RouterView/>
      </div>
    );
  }
});

module.exports = AppView;