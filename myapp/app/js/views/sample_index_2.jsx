'use strict';

var React = require('react');

var SampleIndex2 = React.createClass({

  componentDidMount: function(){
  },

  render: function(){
    return (
      <div className="app-view">
        This is a sample index 2.
      </div>
    );
  }
});

module.exports = SampleIndex2;