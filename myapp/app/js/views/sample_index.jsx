'use strict';

var React = require('react');
var MyTable = require('./my_table.jsx');
var api = require('../api/user');

var SampleIndex = React.createClass({

  componentDidMount: function(){
    api.getAllPosts().then( (data) => {
      this.setState({posts: data});
    }, (err) => {
      actions.goTo(routes.STATIC_INDEX);
      console.log(err);
    }).catch( (err) => {
      if (err instanceof Error) {
        throw err;
      } else {
        console.log(err);
      }
    });
  },

  getInitialState: function(){
    return {posts: []}
  },


  render: function(){
    return (
      <div className="app-view">
        <div>
          <b>Congratulations</b>, you are now able to see some data in a table.
        </div>
        <p>
          Table below:
        </p>
        <MyTable posts={this.state.posts}/>
      </div>
    );
  }
});

module.exports = SampleIndex;