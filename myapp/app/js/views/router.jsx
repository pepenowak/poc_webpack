"use strict";
var React = require('react'),
  routes = require('../routes'),
  utils = require('../utils'),
  page = require('page'),
  actions = require('../actions'),
  Promise = require('bluebird'),
  api = require('../api/user');

var SampleIndex = require('./sample_index.jsx');
var SampleIndex2 = require('./sample_index_2.jsx');

/**
 * Routes handlers
 * ----------------------------------------------------------------------
 */


var _handlers = {

  user: function (ctx, next) {
    ctx._reactState = ctx._reactState || {};
    ctx._reactState.route = routes.STATIC_INDEX;
    ctx._reactState.title = "User INDEX";
    this.setState(ctx._reactState);
  },

  index2: function (ctx, next) {
    ctx._reactState = ctx._reactState || {};
    ctx._reactState.route = routes.INDEX2;
    ctx._reactState.title = "User INDEX - TWO";
    this.setState(ctx._reactState);
  },

  //onGetAllPosts: function(ctx){
  //
  //  ctx._reactState = ctx._reactState || {};
  //  api.getAllPosts().then( (data) => {
  //    console.log(data);
  //    var posts = data;
  //    ctx._reactState.route = routes.POSTS;
  //    ctx._reactState.title = "Posts";
  //    ctx._reactState.context = {};
  //    ctx._reactState.context.executions = posts;
  //    this.setState(ctx._reactState);
  //  }, (err) => {
  //    actions.goTo(routes.STATIC_INDEX);
  //    console.log(err);
  //  }).catch( (err) => {
  //    if (err instanceof Error) {
  //      throw err;
  //    } else {
  //      console.log(err);
  //    }
  //  });
  //},

  // Fallback route
  default: function (ctx) {
    ctx._reactState = ctx._reactState || {};
    this.setState({
      route: ctx.canonicalPath,
      context: { query: ctx.querystring },
      title: "Sample Application",
      realm: ctx._reactState.realm
    });
  }
};


/**
 * View resolver for routes
 * -----------------------------------------------------------------------
 */

var _resolvers = {
  default: function () {
    return <SampleIndex/>;
  }
};

_resolvers[routes.USER] = function() {
  return <SampleIndex/>
};

_resolvers[routes.INDEX2] = function() {
  return <SampleIndex2/>
};

/**
 * Component
 * -----------------------------------------------------------------------
 */

var RouterView = React.createClass({

  getDefaultProps: function () {},

  getInitialState: function () {
    return {
      route: routes.STATIC_INDEX,
      context: {},
      title: "Sample Application",
      realm: ''
    };
  },

  resolveComponent: function () {
    return (
    this.resolvers[this.state.route] || this.resolvers.default
    )();
  },

  componentDidUpdate: function () {
    document.title = this.state.title;
  },

  componentWillMount: function () {
    this.handlers = utils.oop.bind(_handlers, this);
    this.resolvers = utils.oop.bind(_resolvers, this);
  },

  componentDidMount: function () {

    // Add handlers for routes in this stage, handlers can be chained and
    // receive data from previous handlers (see the admin handler for example)
    // They should populate the state of the router component using
    // this.setState to TERMINATE processing as this will trigger a render
    // cycle which will use resolveComponent to infer the correct view
    // from the state

    // Base handlers (users)
    page(routes.STATIC_INDEX, this.handlers.user);
    page(routes.USER, this.handlers.user);
    page(routes.INDEX2, this.handlers.index2);
    //page(routes.EXECUTIONS, this.handlers.onGetExecutions);

    //Fallback
    page('*', this.handlers.default);

    page.start();

    // Listen to the goTo actions which will switch routes, always use goTo to
    // change page, never directly call the page object
    actions.goTo.listen( (route) => {
      console.log(`[ROUTER] switching to ${route}`);
      page(route);
    });
  },

  componentWillUnmount: function () {
    page.stop();
  },


  render: function () {

    return (
      <div className="main-view">
        <div className="router">{ this.resolveComponent() }</div>
      </div>
    );
  }

});

module.exports = RouterView;