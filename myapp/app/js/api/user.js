"use strict";

var Api = require('./api'),
  common = require('./common'),
  _ = require('./../utils').lodash;

var _api = new Api('');


_api.add('getAllPosts', function() {
  return this.simpleRequest(this.url('posts'));
});

_api.errorWrapper = function (reject, func) {
  return (err, res) => {

    var isUnauthorised = (
    res && res.body && res.body.non_field_errors && res.body.non_field_errors[0] === "Unable to log in with provided credentials.");

    if(res && res.body && res.error){
      return reject(new common.APIError(res.text, res.body.error))
    }

    if(isUnauthorised){
      return reject(
        new common.APIError(
          '[Unauthorised access]', res.status));
    }

    return this.baseErrorWrapper(reject, func)(err, res);
  }
}

module.exports = _api;
