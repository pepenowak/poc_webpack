"use strict";

var Promise = require('bluebird');

var common = require('./common');

/**
 * @class Api
 * Wrapper class to create api objects
 * the realm is used to prefix all the endpoints managed by the instance
 */
function Api (realm=undefined) {
  this.token = undefined;
  this.realm = (realm ? ['http://jsonplaceholder.typicode.com', realm] : ['http://jsonplaceholder.typicode.com']).join('/');
}

/**
 * Transforms an arbitrary list of strings into a url path
 * where the root is the instance url
 *
 * @param {string...}
 * @return {string}
 */
Api.prototype.url = function () {
  return [this.realm, ...arguments].join('/');
};

/**
 * Adds a method to an existing instance under the given name and binds it
 * to the instance
 * @param {string} name
 * @param {Function} func
 */
Api.prototype.add = function (name, func) {
  this[name] = func.bind(this);
};

/**
 * Add a formatted 'Authorization' header to a request object
 * the header is of form 'Token {token}'
 * @param  {Request} req    superagent request object
 * @return {Request}        Updated superagent request
 * @throws {APIError}        when token is undefined
 */
Api.prototype.authenticate = function (req) {
  return req;
};


/**
 * Simple promise resolving with the received data on GET request
 * @param {string} url
 * @param {Object} payload
 * @param {Function} auth   auth function if needed (defaults to undefined)
 * @return Promise
 */
Api.prototype.simpleRequest = function (url, payload={}) {

  return new Promise((resolve, reject) => {

    // Create the request with suitable headers
    var req = this.authenticate(common.bootstrapJSONRequest(url));
    // Adds the payload as query parameters (GET request)
    req.query(payload);

    // Do the HTTP request and wraps error handling
    req.end(this.errorWrapper(reject, (err, res) => {

      resolve(res.body);
    }));
  });
};

/**
* Wraps a function with standard error handling for use in Promises
* @param {Function} reject
* @param {Function} func
*/
Api.prototype.errorWrapper = function (reject, func) {
  return (err, res) => {
    return this.baseErrorWrapper(reject, func)(err, res);
  }
};

Api.prototype.baseErrorWrapper = function (reject, func) {
  return function (err, res) {
    if (err) {
      if (err.response.body.error) {
        return reject(new common.APIError(err.response.body.error, err.status));
      } else {
        return reject(new common.APIError(err.response.text, err.status));
      }
    } else if (res.error && res.body && res.body.error) {
      return reject(new common.APIError(res.body.error, res.status));
    } else {
      return func(err, res);
    }
  };
};

module.exports = Api;
