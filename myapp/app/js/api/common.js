"use strict";

var request = require('superagent');
var extend = require('../utils/oop').extend;

/**
 * Creates a Request from url and  add JSON content type and accept headers
 * @param {string} url
 * @param {string} method   HTTP method to use as accepted by superagent (defaults to GET)
 * @return {Request}
 */
function bootstrapJSONRequest (url, method='GET') {
  return request(method, url)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json');
}

function deleteBootstrapJSONRequest(url) {
  var method = "DELETE";
  return request(method, url)
    .set("Content-Type", "application/json")
    .set("Accept", "application/json");
}

/**
 * @class APIError
 * @param {string} text
 * @param {Integer} code
 */
function APIError (text, code=undefined) {
  this.name = "APIError";
  this.message = `${text} ${code ? ' (' + code  + ')' : ''}`.trim();
  this.text = text;
  this.code = code;
}
extend(APIError, Error);

module.exports = {
  bootstrapJSONRequest, deleteBootstrapJSONRequest, APIError
};
