/**
 * @module utils/dom
 *
 * Replaces common DOM related operations usually done through jQuery
 */
"use strict";

var $ = {};

/**
 * @function hasClass
 *
 * Test if given DOM element has class className
 *
 * @param  {DOMElement}  elem
 * @param  {String}  className
 * @return {boolean}
 */
$.hasClass = function hasClass (elem, className) {
  if (elem.classList) {
    return elem.classList.contains(className);
  } else {
    return elem.className.indexOf(className) > -1;
  }
};

/**
 * Add a class if not there, remove otherwise
 * @param  {DOMElement}  elem
 * @param  {String}  className
 */
$.toggleClass = function toggleClass (elem, className) {
  if (elem.classList) {
    return elem.classList.toggle(className);
  }

  return $.hasClass(elem, className) ?
    $.removeClass(elem, className) : $.addClass(elem, className);

};

/**
 * Add className to element
 * @param  {DOMElement}  elem
 * @param  {String}  className
 * @return {boolean}
 */
$.addClass = function addClass (elem, className) {
  if ($.hasClass(elem, className)) {
    return;
  }

  if (elem.classList) {
    elem.classList.add(className);
  } else {
    elem.className += ' ' + className;
  }
};

/**
 * Remove className to element, fails silently
 * @param  {DOMElement}  elem
 * @param  {String}  className
 * @return {boolean}
 */
$.removeClass = function removeClass (elem, className) {
  if (!$.hasClass(elem, className)) {
    return;
  }

  if (elem.classList) {
    elem.classList.remove(className);
  } else {
    var classes = elem.className.split(' ');
    var index = classes.indexOf(className);
    if ( index > -1) {
      classes.splice(index, 1);
    }
    elem.className = classes.join(' ');
  }
};

/**
 * Add an event callback to an element
 * @param {DOMElement}   element
 * @param {string}   eventName
 * @param {Function} callback
 * @param {boolean}   useCapture
 */
$.addEvent = function addEvent (element, eventName, callback, useCapture) {
  if (element.addEventListener) {
    element.addEventListener(eventName, callback, useCapture || false);
  } else if (element.attachEvent) { // Internet Explorer specific
    element.attachEvent("on" + eventName, callback);
  } else {
    element["on" + eventName] = callback;
  }
};

/**
 * Remove an event callback from an element
 * @param {DOMElement}   element
 * @param {string}   eventName
 * @param {Function} callback
 * @param {boolean}   useCapture
 */
$.removeEvent = function removeEvent (element, eventName, callback, useCapture) {
  if (element.removeEventListener) {
    element.removeEventListener("" + eventName, callback, useCapture);
  } else if (element.detachEvent) { // Internet Explorer specific
    element.detachEvent("on" + eventName, callback);
  } else {
    element["on" + eventName] = void 0;
  }
};

/**
 * Prevent default event listeners from being called
 * @param {Event} event
 */
$.preventDefault = function preventDefault (event) {
  if (event.preventDefault) {
    event.preventDefault();
  } else {
    event.returnValue = false;
  }
};

module.exports = $;
