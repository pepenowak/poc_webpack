"use strict";

var Promise = require('bluebird');

module.exports.oop = require('./oop');
module.exports.dom = require('./dom');
module.exports.Schema = require('./schema');
module.exports.validation = require('./validation');
module.exports.lodash = require('lodash');

/**
 * Return a pseudo random id by concatenating current time and a 4 digits
 * random integer
 *
 * @return {Integer}
 */
module.exports.randomId = function () {
  return (new Date()).getTime() + Math.floor(Math.random()) * 1000;
};

/**
 * Empty promise to simulate Promise-like behaviour in a synchronous context
 * (return when the caller expects a Promise)
 *
 * @return {Promise}
 */
module.exports.passThroughPromise = function () {
  return new Promise(function (resolve) { resolve(); });
};

module.exports.download = function(filename, text){
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
};

module.exports.convertDateToString = function(timestamp){
  var date = new Date(timestamp);
  return date.toString();
}