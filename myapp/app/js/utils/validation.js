"use strict";
var _ = require('lodash');

module.exports.isInt = function(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
};

module.exports.isPositiveInt = function(value) {
  return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value)) && value > 0
};

module.exports.isNonEmpty = function(value) {
  return value.length > 0;
};

module.exports.isEmpty = function(value){
  return value === null || value.length===0;
};

module.exports.isIntegerOrIsEmpty = function(value) {
  return (!isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value)) )|| value.length===0;
};

module.exports.limitCharacters = function(numCharacters, value) {
  return value.length > 0 && value.length < numCharacters;
};

module.exports.isPositiveFloat=function(n){
  return n == Number(n) && Number(n) > 0;
};