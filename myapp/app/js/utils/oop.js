"use strict";

/**
 * Syntactic sugar for extending classes
 * @param  {function} child  [child class]
 * @param  {function} parent [class to extend]
 * @param  {object} props  [static properties to add onto child's prototype]
 * @return {function}        [child]
 *
 * @throws {TypeError}
 */
module.exports.extend = function extend (child, parent, props) {

  if (typeof child !== 'function') {
    throw new TypeError(`${child} must be a function`);
  }

  if (typeof parent !== 'function') {
    throw new TypeError(`${parent} must be a function`);
  }

  child.prototype = Object.create(parent.prototype);
  child.prototype.constructor = child;

  if (props && props instanceof Object) {
    Object.keys(props).forEach( function (prop) {
      child.prototype[prop] = props[prop];
    });
  }
};

/**
 * Autobind a dictionnary of function to a given this object
 */
module.exports.bind = function (funcs, _this, assign=false) {
  var bound = {};
  Object.keys(funcs).forEach(function (key) {
    bound[key] = funcs[key].bind(_this);
    if (assign) {
      _this[key] = bound[key];
    }
  });
  return bound;
};