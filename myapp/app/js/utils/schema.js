"use strict";

var _ = require('lodash');

function _identity (val) {
  return val;
}

/**
 *
 * @class Schema
 *
 * Thin wrapper around serializable data schemas
 *
 * Uses JSON.stringify to serialize and a 2 steps process to deserialize
 *
 * A schema is an object indexed by the keys you want available. The members
 * have the following form:
 *
 * + default: value or function used to compite default value, doesn't accept
 * 	 any arguments
 * + deserialize: function, accepts the JSON.parse output for this key and
 * 	 returns the fully deserialized object
 * + serialize: function, accepts a value and returns a serialized version as
 * 	 expected by deserialize
 * + validate: accepts a value and returns an array [boolean, value] where
 * 	 boolean is true if the arguments validates and value is the potentially,
 * 	 sanitized value
 *
 * All 4 are optional and by default no validation/sanitaztion is done.
 * All values default to undefined if not specified otherwise.
 * Default serialization/deserialization is JSON.stringify and JSON.parse
 *
 * Object, keys and schema parameters are frozen after instance is created
 * so only the actual fields can be modified.
 *
 * If lsIdentifier is passed, it is possible to read/write the schema in
 * localStorage
 *
 * @param {Object} schema
 * @throws TypeError on non function deserialize, serialize or validate
 */
function Schema (schema={}, lsIdentifier=undefined) {

  this._keys = Object.keys(schema);
  this._schema = schema;

  this._lsIdentifier = lsIdentifier;

  this._keys.forEach((key) => {

    var _s = this._schema[key];

    if (typeof _s.default !== 'function') {
      _s.default = _identity.bind(this, _s.default || undefined);
    }

    if (_s.deserialize && typeof _s.deserialize !== 'function') {
      throw new TypeError(`deserialize must be a function (on key ${key})`);
    }

    if (_s.serialize && typeof _s.serialize !== 'function') {
      throw new TypeError(`serialize must be a function (on key ${key})`);
    }

    if (_s.validate && typeof _s.validate !== 'function') {
      throw new TypeError(`validate must be a function (on key ${key})`);
    }

    this[key] = _s.default();
  });

  // Lock up schema after creation
  Object.preventExtensions(this);
  Object.freeze(this._schema);
  Object.freeze(this._keys);
}

/**
 * @return {Object} object containing only the schema's keys
 */
Schema.prototype.data = function () {
  return _.pick(this, this._keys);
}

/**
 * @param {string} key
 * @return {boolean}
 */
Schema.prototype.hasKey = function (key) {
  return this._keys.indexOf(key) > -1;
}

/**
 * Resets all the keys to their default value
 */
Schema.prototype.reset = function () {
  this._keys.forEach((key) => {
    this[key] = this._schema[key].default();
  });
}

/**
 * Resets as well as clear the local storage object if applicable
 */
Schema.prototype.clear = function () {
  this.reset();
  this.clearStorage();
}

/**
 * @param  {String} key
 * @return {}
 */
Schema.prototype.get = function (key) {
  return this.hasKey(key) ? this[key] : undefined;
}


/**
 *
 * Validate and sanitize a value for a specific key, use the validate field
 * of the schema or passthrough if not set.
 *
 * @param  {String} key
 * @param  {Object} value
 * @return {boolean}       [description]
 */
Schema.prototype._validate = function (key, value) {

  if (this._schema[key].validate) {
    var [valid, newValue] = this._schema[key].validate(value);
    if (valid) {
      return newValue;
    } else {
      throw new Error(
        `${value} (${typeof value}) is not a valid value for key ${key}`);
    }
  } else {
    return value;
  }

}

/**
 * @param {string} key
 * @param {Object} value
 */
Schema.prototype.set = function (key, value) {

  if (!this.hasKey(key)) {
    throw new ReferenceError(`Key ${key} doesn't exist`);
  }

  this[key] = this._validate(key, value);
}

/**
 * @param {Object} data [set of key/value pairs]
 */
Schema.prototype.setAll = function (data) {

  if (!_.isPlainObject(data)) {
    throw new TypeError("Can only accept plain objects");
  }

  this._keys.forEach((key) => {
    if (data.hasOwnProperty(key)) {
      this.set(key, data[key]);
    }
  });
}

/**
 * Serialize the schema using either the serialize field for each key or the
 * toJson function of the individual objects.
 *
 * Runs the final object through JSON.stringify to be usable in AJAX request
 * or for storing
 *
 * @return {string}
 */
Schema.prototype.serialize = function () {
  var plain = this.data();
  this._keys.forEach((key) => {
    if (this._schema[key].serialize) {
      plain[key] = this._schema[key].serialize(plain[key]);
    } else if (_.isObject(plain[key]) && _.isFunction(plain[key].toJson)) {
      plain[key] = plain[key].toJson();
    }
  });

  return JSON.stringify(plain);
}

/**
 * Deserializes a source object.
 *
 * Source must be a plain object or a string (coercible to plain objects by
 * JSON.parse)
 *
 * All keys are run through their deserialize field or returned as parsed, the
 * update flag will pass the results to this.setAll while the partial flag
 * allows for missing keys (default behaviour). Setting partial to false will
 * add the default value for missing keys.
 *
 * @param  {(Object|String)} source
 * @param  {Boolean} partial
 * @param  {Boolean} update
 * @return {Object}
 * @throws {TypeError}
 */
Schema.prototype.deserialize = function (
  source, {partial=true, update=false}={}) {  // jshint ignore:line

  if (!source) { return {}; }

  var data;

  // Coerce source to a plain object or throw an exception
  if (!_.isPlainObject(source)) {

    if (_.isString(source)) {
      try {
        data = JSON.parse(source);
      } catch (e) {}
    }

    if (data === undefined) {
      throw new TypeError("Can only deserialize JSON strings or plain objects");
    }
  } else {
    data = source;
  }

  // Perform deserialization
  var value,
    output = {};

  this._keys.forEach((key) => {

    if (data.hasOwnProperty(key)) {

      value = data[key];

      value = (
        this._schema[key].deserialize ?
          this._schema[key].deserialize(value) : value);

      output[key] = this._validate(key, value);

    } else if(!partial) { // Add defaults if partial flag is not set
      output[key] = this._schema[key].default();
    }
  });

  // Update instance
  if (update) {
    this.setAll(output);
  }

  return output;
}

/**
 * Write to localStorage if applicable
 */
Schema.prototype.save = function () {
  if (this._lsIdentifier && window.localStorage) {
    window.localStorage.setItem(this._lsIdentifier, this.serialize());
  }
}

/**
 * Load from localStorage if applicable
 * Use this.deserialize to load data
 */
Schema.prototype.load = function () {
  if (this._lsIdentifier && window.localStorage) {
    var source = window.localStorage.getItem(this._lsIdentifier);
    this.deserialize(source, {partial: false, update: true});
  }
}

/**
 * Clear localStorage object if applicable
 */
Schema.prototype.clearStorage = function () {
  if (this._lsIdentifier && window.localStorage) {
    window.localStorage.removeItem(this._lsIdentifier);
  }
}

module.exports = Schema;
