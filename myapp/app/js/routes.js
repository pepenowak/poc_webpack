"use strict";

var routes = {

  //TODO: For routing to work, we need to setup server that serves index.html on every URL request
  NULL: '',
  STATIC_INDEX: '/',
  USER: '/user',
  INDEX2: '/index2'
};

module.exports = routes;