"use strict";

var React = require('react');
var ReactDOM = require('react-dom');
var AppView = require('./js/views/app.jsx');

ReactDOM.render(React.createElement(AppView, null), document.getElementById('content'));
